<%--
  Created by IntelliJ IDEA.
  User: wfoudijk
  Date: 11-12-18
  Time: 8:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JS demo</title>
    <link rel="stylesheet" href="css/main.css">
    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/lib/jquery-3.3.1.min.js" charset="utf-8"></script>
</head>
<body>
<h1>Hello java script</h1>
<div id="movie_review">
    This piece is about .. Click <a id="show_spoiler" href="#">here</a> for the spoiler
</div>

<div id="movie_spoiler"> The bad guy dies</div>

</body>
</html>
