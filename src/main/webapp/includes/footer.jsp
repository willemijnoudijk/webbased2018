<%--
  Created by IntelliJ IDEA.
  User: wfoudijk
  Date: 5-12-18
  Time: 8:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="footer">
    Copyright &copy; 2018 Michiel Noback <br />
    Please contact Henk at the Hanze for royalties <br />
    email: ${param.admin_email} <br />
</div>
