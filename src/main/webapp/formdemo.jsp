<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wfoudijk
  Date: 13-12-18
  Time: 11:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Form demo</title>
</head>
<body>
<jsp:include page="includes/banner.jsp">
    <jsp:param name="user.role" value="${requestScope.user.role}"></jsp:param>
</jsp:include>
<form action="handleform" method="post">
    <input type="number" value="1"/> <br/>
    <input type="date" name="date"/> <br />
    <select name= "bfv class" >
        <c:forEach var="bfv_class" items="${requestScope.classes}">
            <option value="${bfv_class}">${bfv_class}</option>
        </c:forEach>

    </select> <br />
    <label>female</label><input type="radio" name="gender" value="female">
    <label>male</label><input type="radio" name="gender" value="male"> <br/>
    <label>one</label><input type="checkbox" name="check" value="one"/> <br/>
    <label>two</label><input type="checkbox" name="check" value="two"/> <br/>

    <input type="submit" value="do it"/>

</form>
</body>
</html>
