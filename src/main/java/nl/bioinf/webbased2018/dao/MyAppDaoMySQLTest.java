package nl.bioinf.webbased2018.dao;
import nl.bioinf.webbased2018.users.Role;
import nl.bioinf.webbased2018.users.User;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class MyAppDaoMySQLTest {
    private MyAppDaoMySQL dao;

    @Before
    public void before() {
        this.dao = MyAppDaoMySQL.getInstance();
        try {
            dao.connect();
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void connect() {
        assertFalse(dao.connection == null);
        try {
            assertTrue(! dao.connection.isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserSunny() {
        try {
            User user = dao.getUser("Henk", "henkieissafe");
            assert(user != null);
            assert(user.getUserName().equals("Henk"));
            assert(user.getRole() == Role.USER);
            assert(user.getEmail().equals("henk@example.com"));
        } catch (DatabaseException e) {
            e.printStackTrace();
            assert(false);
        }
    }

    @Test
    public void getUserUnknown() {
        try {
            User user = dao.getUser("Piet", "pietepiet");
            assert(user == null);
        } catch (DatabaseException e) {
            e.printStackTrace();
            assert(false);
        }
    }

    @Ignore
    @Test
    public void insertUser() {
        String name = "Klaas";
        String pass = "sinterklaas";
        String email = "sint@example.com";
        Role role = Role.GUEST;
        try {
            dao.insertUser(name, pass, email, role);

            User user = dao.getUser(name, pass);
            assert(user != null);
            assert(user.getUserName().equals(name));
            assert(user.getRole() == Role.GUEST);
            assert(user.getEmail().equals(email));

        } catch (DatabaseException e) {
            e.printStackTrace();
            assert(false);
        }

    }

    @Test
    public void disconnect() {
        try {
            dao.disconnect();
            assert(dao == null || dao.connection.isClosed());
        } catch (SQLException e) {
            e.printStackTrace();
            assert(false);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
    }
}
