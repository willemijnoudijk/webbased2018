package nl.bioinf.webbased2018.dao;
import nl.bioinf.webbased2018.users.Role;
import nl.bioinf.webbased2018.users.User;

public interface MyAppDao {
    /**
     * connects to the data layer.
     * @throws DatabaseException
     */
    void connect() throws DatabaseException;

    /**
     * fetches a user by username and password.
     * @param userName
     * @param userPass
     * @return
     * @throws DatabaseException
     */
    User getUser(String userName, String userPass) throws DatabaseException;

    /**
     * inserts a new User.
     * @param userName
     * @param userPass
     * @param email
     * @param role
     * @throws DatabaseException
     */
    void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException;

    /**
     * closes the connection to the data layer and frees resources.
     * @throws DatabaseException
     */
    void disconnect() throws DatabaseException;
}
