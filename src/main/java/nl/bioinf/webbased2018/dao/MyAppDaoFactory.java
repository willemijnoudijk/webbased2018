package nl.bioinf.webbased2018.dao;

public class MyAppDaoFactory {
    /*
    given String requesting type of database; return that correct type
     */
    public static MyAppDao getDatasource(String type) {
        switch(type) {
            case "dummy": return new MyAppDaoInMemory();
            case "mysql": return MyAppDaoMySQL.getInstance();
            default: throw new IllegalArgumentException("unknown database type requested");
        }

    }

}
