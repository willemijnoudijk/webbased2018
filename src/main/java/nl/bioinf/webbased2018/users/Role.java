package nl.bioinf.webbased2018.users;

public enum Role {
    GUEST,
    USER,
    ADMIN;
}
