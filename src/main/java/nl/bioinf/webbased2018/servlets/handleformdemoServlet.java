package nl.bioinf.webbased2018.servlets;

import nl.bioinf.webbased2018.users.Role;
import nl.bioinf.webbased2018.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "handleformdemoServlet", urlPatterns = "/handleform")
public class handleformdemoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String number = request.getParameter("number");
        System.out.println(number);
        String date = request.getParameter("date");
        String BFV = request.getParameter("bfv class");
        String check = request.getParameter("check");
        System.out.println(date);
        System.out.println(BFV);
        System.out.println(check);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<String> classes = new ArrayList<>();
        classes.add("BFV1");
        classes.add("BFV2");
        classes.add("BFV3");
        classes.add("BFV4");
        request.setAttribute("classes", classes);
        request.setAttribute("user", new User("Michiel", "Fubar", "michiel@bioinf.nl", Role.ADMIN));
        final RequestDispatcher requestDispatcher = request.getRequestDispatcher("formdemo.jsp");
        requestDispatcher.forward(request, response);



    }
}
