package nl.bioinf.webbased2018.servlets;

import nl.bioinf.webbased2018.users.Role;
import nl.bioinf.webbased2018.users.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet", urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        //session.setMaxInactiveInterval(2 * 60 * 60); // two hours
        session.setMaxInactiveInterval(60);

        RequestDispatcher view;
        User u = (User) session.getAttribute("user");
        if (u != null) {
            view = request.getRequestDispatcher("Main.jsp");
        } else {
            //authenticate
            String username = request.getParameter("username");
            String password = request.getParameter("password");

            final User authenticatedUser = authenticate(username, password);
            if (authenticatedUser == User.GUEST) {
                final String errorMessage = "Invalid username and/or password";
                request.setAttribute("errorMessage", errorMessage);
                view = request.getRequestDispatcher("Login.jsp");
            } else {
                session.setAttribute("user", authenticatedUser);
                view = request.getRequestDispatcher("Main.jsp");
            }
        }
        view.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User u = (User) request.getSession().getAttribute("user");
        RequestDispatcher view;
        if (u != null) {
            view = request.getRequestDispatcher("Main.jsp");
        } else {
            view = request.getRequestDispatcher("Login.jsp");
        }
        view.forward(request, response);
    }

    private User authenticate(String username, String password) {
        //System.out.println("test");
        if (username.equals("Henk") && password.equals("henk")) {
//            User henk = new User(username, password, "henk@henk.nl", Role.USER);
            return new User(username, password, "henk@henk.nl", Role.USER);
        } else {
            return User.GUEST;
        }
    }

}
