package nl.bioinf.webbased2018.servlets;

import nl.bioinf.webbased2018.messages.MessageFactory;

//import javax.jms.Message;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

@WebServlet(name = "HelloServlet", urlPatterns = "/hello")
public class HelloServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        handleRequest(request, response);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        handleRequest(request, response);
    }

    private void handleRequest(HttpServletRequest request,
                               HttpServletResponse response)
            throws IOException, ServletException {
//        PrintWriter out = response.getWriter();
//        LocalDateTime currentTime = LocalDateTime.now();
//        String message = "Welcome, it is now " + currentTime.toLocalDate();
//        request.setAttribute("servletmessage", message);

        final String message = MessageFactory.getMessage();
        request.setAttribute("daily_message", message);
        RequestDispatcher view = request.getRequestDispatcher("hello.jsp");
        view.forward(request, response);
//        out.println("<html>");
//        out.println("<head><title>My Hello World jsp</title>" +
//                "</head><body><h1>It is ");
//        out.println(currentTime.toString());
//        out.println(" O’ Clock</h1></body></html>");
    }

}
