package nl.bioinf.webbased2018.messages;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Locale;

public abstract class MessageFactory {

    public static String getMessage() {
        LocalDateTime currentTime = LocalDateTime.now();
        DayOfWeek dayOfWeek = currentTime.getDayOfWeek();
        String day = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);//getDefault());
        String message = "Today is a " + day;
        switch (dayOfWeek.getValue()) {
            case 6: {
                return message += "; working on a " + day + "? Get a life!";
            }
            case 7: {
                return message += "; working on a " + day + "? You infidel!";
            }
            default: {
                return message += "; working like an office zombie...";
            }
        }
    }
}
