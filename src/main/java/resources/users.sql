drop table if exists Users;

        create table Users (
        user_id int auto_increment not null,
        user_name varchar(50) not null unique,
        user_email varchar(100),
        user_password varchar(100) not null,
        user_role enum ('GUEST','USER','ADMIN'),
        primary key (user_id)
        );

        insert into Users (user_name, user_email, user_password, user_role) values
        ("Henk", "henk@example.com", "henkieissafe", "USER");
        insert into Users (user_name, user_email, user_password, user_role) values
        ("Floor", "floor@example.com", "floortje", "ADMIN");