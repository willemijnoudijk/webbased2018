<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: wfoudijk
  Date: 27-11-18
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Nucleotide</title>
</head>
<body>
<p>
<h1>the letter A stands for: ${requestScope.NucMap.A}</h1>

<table>
    <c:forEach var="nucleotide" items="${requestScope.NucMap}">
        <tr>
            <td>${nucleotide.key}</td>
            <td>${nucleotide.value}</td>
        </tr>
    </c:forEach>
</table>


</p>

</html>
